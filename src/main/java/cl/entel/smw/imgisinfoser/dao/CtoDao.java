package cl.entel.smw.imgisinfoser.dao;

import cl.entel.smw.imgisinfoser.client.model.*;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CtoDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

 //   public List<Mte> getCtoBySplitterByQuery(String cardUrn) {
 //       List<Mte> result = new ArrayList<>();
 //       try {
 //           result = jdbcTemplate.query("select a.id, a.NAME nombre from\n" +
 //                   "                mit_terminal_enclosure a,\n" +
 //                   "                pra_admin.pracard spli,\n" +
 //                   "                pra_admin.praequipmentholder slot,\n" +
 //                   "                pra_admin.praequipmentholder shelf,\n" +
 //                   "                pra_admin.praequipmentholder bay,\n" +
 //                   "                pra_admin.praequipmentholder eh\n" +
 //                   "where\n" +
 //                   "        spli.objectid = ?\n" +
 //                   "  and spli.a_equipmentholder_reid = slot.reid\n" +
 //                   "  and slot.holdercompositeparent_reid = shelf.reid\n" +
 //                   "  and shelf.holdercompositeparent_reid = bay.reid\n" +
 //                   "  and bay.holdercompositeparent_reid = eh.reid\n" +
 //                   "  and a.clli = substr(eh.OBJECTID, 65,8)", new CtoRowmapper(), cardUrn);
 //       } catch (EmptyResultDataAccessException ignored) {}
//
 //       return result;
 //   }
    public List<Mte> getMteByIdteByProcedure(String varTE,String field) {
        List<Mte> result = new ArrayList<>();
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("P_IMGIS_MEJORAS_GPON")
                .withProcedureName("get_TerminalEnclosure")
                .declareParameters(
                        new SqlParameter("varTE", OracleTypes.VARCHAR),
                        new SqlParameter("field", OracleTypes.VARCHAR),
                        new SqlOutParameter("curCtos", OracleTypes.CURSOR)
                );
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("varTE", varTE);
        in.addValue("field", field);

        Map<String, Object> out = simpleJdbcCall.execute(in);

        for (Map<String, Object> registry : (List<Map>) out.get("curCtos")) {

            Date date1 = (Date) registry.get("FECHA_ENTREGA");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

            String strDate1 = "";

            if (date1 != null) {
                strDate1 = dateFormat.format(date1);
            } else {
                strDate1 = "";
            }
            Mte a_mte = new Mte();
            a_mte.setId((BigDecimal) registry.get("ID"));
            a_mte.setName((String) registry.get("NAME"));
            a_mte.setPais((String) registry.get("PAIS"));
            a_mte.setRegion((String) registry.get("REGION"));
            a_mte.setComuna((String) registry.get("COMUNA"));
            a_mte.setCalle((String) registry.get("CALLE"));
            a_mte.setAltura((BigDecimal) registry.get("ALTURA"));
            a_mte.setSpecId((BigDecimal) registry.get("SPEC_ID"));
            a_mte.setSpecName((String) registry.get("SPEC_NAME"));
            a_mte.setConSta((String) registry.get("CONSTRUCTION_STATUS"));
            a_mte.setFijacion((String) registry.get("FIJACION"));
            a_mte.setAcceso((String) registry.get("ACCESO"));
            a_mte.setFechaEnt( strDate1 );
            a_mte.setProy((String) registry.get("PROYECTO"));
            a_mte.setProp((String) registry.get("PROPIETARIO"));
            a_mte.setLocationAddress((BigDecimal) registry.get("LOCATION_ADDRESS"));

            result.add(a_mte);

        }

        return result;
    }
    public List<Building> getBuiByIdteByProcedure(String idBui) {
        List<Building> result = new ArrayList<>();
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("P_IMGIS_MEJORAS_GPON")
                .withProcedureName("get_Building")
                .declareParameters(
                        new SqlParameter("idBui", OracleTypes.VARCHAR),
                        new SqlOutParameter("curCtos", OracleTypes.CURSOR)
                );
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("idBui", idBui);

        Map<String, Object> out = simpleJdbcCall.execute(in);

        for (Map<String, Object> registry : (List<Map>) out.get("curCtos")) {

            Date date1 = (Date) registry.get("FECHA_ENTREGA");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

            String strDate1 = "";

            if (date1 != null) {
                strDate1 = dateFormat.format(date1);
            } else {
                strDate1 = "";
            }
            Building a_bui = new Building();
            a_bui.setId((BigDecimal) registry.get("ID"));
            a_bui.setName((String) registry.get("NAME"));
            a_bui.setPais((String) registry.get("PAIS"));
            a_bui.setRegion((String) registry.get("REGION"));
            a_bui.setComuna((String) registry.get("COMUNA"));
            a_bui.setCalle((String) registry.get("CALLE"));
            a_bui.setAltura((BigDecimal) registry.get("ALTURA"));
            a_bui.setConstructionStatus((String) registry.get("CONSTRUCTION_STATUS"));
            a_bui.setFijacion((String) registry.get("FIJACION"));
            a_bui.setAcceso((String) registry.get("ACCESO"));
            a_bui.setFechaEntrega( strDate1 );
            a_bui.setProyecto((String) registry.get("PROYECTO"));
            a_bui.setPropietario((String) registry.get("PROPIETARIO"));
            a_bui.setInfraestructura((String) registry.get("INFRAESTRUCTURA"));
            a_bui.setTipoSector((String) registry.get("TIPO_SECTOR"));
            a_bui.setDistribucion((String) registry.get("DISTRIBUCION"));
            a_bui.setPropietarioAcceso((String) registry.get("PROPIETARIO_ACCESO"));
            a_bui.setPisos((BigDecimal) registry.get("PISOS"));
            a_bui.setContrato((String) registry.get("CONTRATO"));
            a_bui.setLocationAddress((BigDecimal) registry.get("LOCATION_ADDRESS"));

            result.add(a_bui);

        }

        return result;
    }

    public List<MHub> getHubByIdteByProcedure(String varHub,String field) {
        List<MHub> result = new ArrayList<>();
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("P_IMGIS_MEJORAS_GPON")
                .withProcedureName("get_MitHub")
                .declareParameters(
                        new SqlParameter("varHub", OracleTypes.VARCHAR),
                        new SqlParameter("field", OracleTypes.VARCHAR),
                        new SqlOutParameter("curCtos", OracleTypes.CURSOR)
                );
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("varHub", varHub);
        in.addValue("field", field);

        Map<String, Object> out = simpleJdbcCall.execute(in);

        for (Map<String, Object> registry : (List<Map>) out.get("curCtos")) {


            MHub a_hub = new MHub();
            a_hub.setId((BigDecimal) registry.get("ID"));
            a_hub.setName((String) registry.get("NAME"));
            a_hub.setPais((String) registry.get("PAIS"));
            a_hub.setRegion((String) registry.get("REGION"));
            a_hub.setComuna((String) registry.get("COMUNA"));
            a_hub.setCalle((String) registry.get("CALLE"));
            a_hub.setAltura((BigDecimal) registry.get("ALTURA"));
            a_hub.setConstructionStatus((String) registry.get("CONSTRUCTION_STATUS"));
            a_hub.setFijacion((String) registry.get("FIJACION"));
            a_hub.setAcceso((String) registry.get("ACCESO"));
            a_hub.setPropietario((String) registry.get("PROPIETARIO"));
            a_hub.setType((String) registry.get("TYPE"));
            a_hub.setTipoNodo((String) registry.get("TIPO_NODO"));
            a_hub.setEstadoRespartidor((String) registry.get("ESTADO_REPARTIDOR"));
            a_hub.setInfraestructura((String) registry.get("INFRAESTRUCTURA"));
            a_hub.setTipoSector((String) registry.get("TIPO_SECTOR"));
            a_hub.setPropietarioAcceso((String) registry.get("PROPIETARIO_ACCESO"));
            a_hub.setDescripcionLugar((String) registry.get("DESC_LUGAR"));
            a_hub.setPiso((BigDecimal) registry.get("PISO"));
            a_hub.setLocationAddress((BigDecimal) registry.get("LOCATION_ADDRESS"));

            result.add(a_hub);

        }

        return result;
    }
    public List<Splitter> getSplitterByFieldByProcedure(String varSpl,String field) {
        List<Splitter> result = new ArrayList<>();
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("P_IMGIS_MEJORAS_GPON")
                .withProcedureName("get_Splitter")
                .declareParameters(
                        new SqlParameter("varSpl", OracleTypes.VARCHAR),
                        new SqlParameter("field", OracleTypes.VARCHAR),
                        new SqlOutParameter("curCtos", OracleTypes.CURSOR)
                );
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("varSpl", varSpl);
        in.addValue("field", field);

        Map<String, Object> out = simpleJdbcCall.execute(in);

        for (Map<String, Object> registry : (List<Map>) out.get("curCtos")) {


            Splitter a_spl = new Splitter();
            a_spl.setId((BigDecimal) registry.get("ID"));
            a_spl.setName((String) registry.get("NAME"));
            a_spl.setSpecName((String) registry.get("SPEC_NAME"));
            a_spl.setFunction((String) registry.get("FUNCTION"));
            a_spl.setConstructionStatus((String) registry.get("CONSTRUCTION_STATUS"));
            a_spl.setNivelCascada((BigDecimal) registry.get("NIVEL_CASCADA"));
            a_spl.setContenedorId((BigDecimal) registry.get("ID_CONTENEDOR"));
            a_spl.setContenedorName((String) registry.get("NAME_CONTENEDOR"));
            a_spl.setContenedorSpec((String) registry.get("SPEC_CONTENEDOR"));
            a_spl.setLocationAddress((BigDecimal) registry.get("LOCATION_ADDRESS"));

            result.add(a_spl);

        }

        return result;
    }
    public List<ResponseGen> getSpecTEByProcedure() {
        List<ResponseGen> result = new ArrayList<>();
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("P_IMGIS_MEJORAS_GPON")
                .withProcedureName("get_tipoSpec")
                .declareParameters(
                        //new SqlParameter("varSpl", OracleTypes.VARCHAR),
                        //new SqlParameter("field", OracleTypes.VARCHAR),
                        new SqlOutParameter("tipoSpec", OracleTypes.CURSOR)
                );
        MapSqlParameterSource in = new MapSqlParameterSource();
        //in.addValue("varSpl", varSpl);
        //in.addValue("field", field);

        Map<String, Object> out = simpleJdbcCall.execute(in);

        for (Map<String, Object> registry : (List<Map>) out.get("tipoSpec")) {


            ResponseGen a_spec = new ResponseGen();
            a_spec.setId((BigDecimal) registry.get("STORED_VALUE"));
            a_spec.setValue((String) registry.get("VALUE"));


            result.add(a_spec);

        }

        return result;
    }
    public List<Odf> getOdfByFieldByProcedure(String varOdf,String field) {
        List<Odf> result = new ArrayList<>();
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("P_IMGIS_MEJORAS_GPON")
                .withProcedureName("get_Odf")
                .declareParameters(
                        new SqlParameter("varOdf", OracleTypes.VARCHAR),
                        new SqlParameter("field", OracleTypes.VARCHAR),
                        new SqlOutParameter("curCtos", OracleTypes.CURSOR)
                );
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("varOdf", varOdf);
        in.addValue("field", field);

        Map<String, Object> out = simpleJdbcCall.execute(in);

        for (Map<String, Object> registry : (List<Map>) out.get("curCtos")) {

            Odf a_odf = new Odf();
            a_odf.setId((BigDecimal) registry.get("ID"));
            a_odf.setName((String) registry.get("NAME"));
            a_odf.setSpecId((BigDecimal) registry.get("SPEC_ID"));
            a_odf.setSpecName((String) registry.get("SPEC_NAME"));
            a_odf.setConstructionStatus((String) registry.get("CONSTRUCTION_STATUS"));
            a_odf.setLadoRepartidor((String) registry.get("LADO_REPARTIDOR"));
            a_odf.setRackName((String) registry.get("NAME_RACK"));
            a_odf.setContenedorId((BigDecimal) registry.get("ID_CONTENEDOR"));
            a_odf.setContenedorName((String) registry.get("NAME_CONTENEDOR"));
            a_odf.setContenedorType((String) registry.get("TYPE_CONTENEDOR"));
            a_odf.setLocationAddress((BigDecimal) registry.get("LOCATION_ADDRESS"));

            result.add(a_odf);

        }

        return result;
    }
}
