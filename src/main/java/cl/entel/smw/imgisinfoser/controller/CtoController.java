package cl.entel.smw.imgisinfoser.controller;

import cl.entel.smw.imgisinfoser.client.model.*;
import cl.entel.smw.imgisinfoser.dao.CtoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CtoController {

    @Autowired
    private CtoDao ctoDao;

    @GetMapping("/imgis/get_TerminalEnclosureByField/{field}/{varTE}")
    private List<Mte> getMteByField(@PathVariable String varTE,@PathVariable String field) {
        return ctoDao.getMteByIdteByProcedure(varTE,field);
    }

    @GetMapping("/imgis/get_Building/{idBui}")
    private List<Building> getBuiById(@PathVariable String idBui) {
        return ctoDao.getBuiByIdteByProcedure(idBui);
    }

    @GetMapping("/imgis/get_HubByField/{field}/{varHub}")
    private List<MHub> getHubByField(@PathVariable String varHub,@PathVariable String field) {
        return ctoDao.getHubByIdteByProcedure(varHub,field);
    }
    @GetMapping("/imgis/get_SplitterByField/{field}/{varSpl}")
    private List<Splitter> getSplitterByField(@PathVariable String varSpl,@PathVariable String field) {
        return ctoDao.getSplitterByFieldByProcedure(varSpl,field);
    }

    @GetMapping("/imgis/get_SpecTE")
    private List<ResponseGen> getSpecTE() {
        return ctoDao.getSpecTEByProcedure();
    }

    @GetMapping("/imgis/get_OdfByField/{field}/{varOdf}")
    private List<Odf> getOdfByField(@PathVariable String varOdf,@PathVariable String field) {
        return ctoDao.getOdfByFieldByProcedure(varOdf,field);
    }

//    @GetMapping("/cto/by/splitterv2/{splitterUrn}")
//    private List<Mte> getCtoBySplitterV2(@PathVariable String splitterUrn) {
//        return ctoDao.getCtoBySplitterByQuery(splitterUrn);
//    }

}
